#import required packages
import pandas as pd
import matplotlib.pyplot as plt

#read the data
df = pd.read_csv("AirQualityUCI.csv", parse_dates=[['Date', 'Time']])

#check the dtypes
df.dtypes