import pandas as pd 

d3 = pd.read_csv('jkh_keyword_gTrend_monthly.csv', parse_dates=["Date"], index_col="Date")
df4 = d3.resample('D').interpolate()
# df4.to_csv('jkh_keyword_gTrend_daily.csv')

d5 = pd.read_csv('net_inflows_cse_monthly.csv', parse_dates=["Date"], index_col="Date")
df6 = d5.resample('D').interpolate()
# df6.to_csv('net_inflows_cse_daily.csv')

d7 = pd.read_csv('real_effective_exchange_rates_monthly.csv', parse_dates=["Date"], index_col="Date")
df8 = d7.resample('D').interpolate()
# df8.to_csv('real_effective_exchange_rates_daily.csv')
